package ds2.artifactory.client.impl;

import ds2.artifactory.client.api.ArtifactoryClientException;
import ds2.artifactory.client.api.dto.ArtifactoryDataDto;
import ds2.artifactory.client.api.json.User;
import ds2.artifactory.client.api.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.lang.invoke.MethodHandles;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

public class UserServiceImplTest {
    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private UserService userService;
    private ArtifactoryDataDto dataDto;

    @BeforeClass
    public void onClass() throws MalformedURLException {
        dataDto = new ArtifactoryDataDto();
//        dataDto.setUserName("mainty");
//        dataDto.setUserPassword("cC758v2oNZWK647HvRzOBIk".toCharArray());
        dataDto.setUserName("admin");
        dataDto.setUserPassword("admin12346".toCharArray());
        dataDto.setServerUrl(new URL("https://artifacts.tignum.com/artifactory"));
        userService = new UserServiceImpl();
    }

    @Test(enabled = false)
    public void unlockUsers() throws ArtifactoryClientException {
        userService.unlockUsers(dataDto, "tignum", "tasks");
    }

    @Test(enabled = false)
    public void listUsers() throws ArtifactoryClientException {
        Collection<User> users = userService.getUsers(dataDto);
        LOG.info("Users: {}", users);
        assertNotNull(users);
        assertTrue(users.size() > 0);
    }
}
