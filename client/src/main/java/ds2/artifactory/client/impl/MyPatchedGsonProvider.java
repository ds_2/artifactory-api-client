package ds2.artifactory.client.impl;

import ds2.jersey.media.json.gson.GsonProvider;

import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

@Provider
@Consumes({MediaType.APPLICATION_JSON, "application/vnd.org.jfrog.artifactory.storage.FolderInfo+json"})
public class MyPatchedGsonProvider<T> extends GsonProvider<T> {
}
