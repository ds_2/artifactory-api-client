package ds2.artifactory.client.impl;

import ds2.artifactory.client.api.ArtifactoryClientException;
import ds2.artifactory.client.api.ArtifactoryData;
import ds2.artifactory.client.api.services.MaintenanceService;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

public class MaintenanceServiceImpl extends AbstractJaxRsBase implements MaintenanceService {
    @Override
    public void startGarbageCollection(ArtifactoryData data) throws ArtifactoryClientException {

    }

    @Override
    public void optimizeSystemStorage(ArtifactoryData data) throws ArtifactoryClientException {
        ///api/system/storage/optimize
        WebTarget target = createTarget(data).path("/api/system/storage/optimize");
        Invocation.Builder builder = applyRequestTypes(target);
        Response response = builder.post(null);
        checkForErrorResponse(response);
        closeResponse(response);
    }
}
