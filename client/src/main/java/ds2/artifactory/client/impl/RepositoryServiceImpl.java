package ds2.artifactory.client.impl;

import ds2.artifactory.client.api.ArtifactoryClientException;
import ds2.artifactory.client.api.ArtifactoryData;
import ds2.artifactory.client.api.Errors;
import ds2.artifactory.client.api.json.FolderInfo;
import ds2.artifactory.client.api.json.RepositoryDetails;
import ds2.artifactory.client.api.json.RepositoryType;
import ds2.artifactory.client.api.json.StorageInfo;
import ds2.artifactory.client.api.services.RepositoryService;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Set;

public class RepositoryServiceImpl extends AbstractJaxRsBase implements RepositoryService {
    @Override
    public void emptyTrashcan(ArtifactoryData data) throws ArtifactoryClientException {
        WebTarget webTarget = createTarget(data).path("/api/trash/empty");
        Invocation.Builder builder = applyRequestTypes(webTarget);
        Response result = builder.post(null);
        if (result == null) {
            throw new ArtifactoryClientException(Errors.NO_RESULT, "No result was received from the server!");
        }
        checkForErrorResponse(result);
        closeResponse(result);
    }

    @Override
    public void calculateMavenMetadata(ArtifactoryData data, String repositoryName, Path folderPath, Boolean recursive) throws ArtifactoryClientException {
        WebTarget webTarget = createTarget(data).path("/api/maven/calculateMetadata").path(repositoryName);
        if (folderPath != null) {
            webTarget = webTarget.path(folderPath.normalize().toString());
        }
        if (recursive != null) {
            webTarget = webTarget.queryParam("nonRecursive", recursive ? "false" : "true");
        }
        Invocation.Builder builder = applyRequestTypes(webTarget);
        Response result = builder.post(null);
        if (result == null) {
            throw new ArtifactoryClientException(Errors.NO_RESULT, "No result was received from the server!");
        }
        checkForErrorResponse(result);
        closeResponse(result);
    }

    @Override
    public FolderInfo getFolderInfo(ArtifactoryData data, String repoKey, Path folderPath) throws ArtifactoryClientException {
        WebTarget webTarget = createTarget(data).path("/api/storage").path(repoKey).path(folderPath.normalize().toString());
        Invocation.Builder builder = applyRequestTypes(webTarget);
        Response result = builder.get();
        if (result == null) {
            throw new ArtifactoryClientException(Errors.NO_RESULT, "No result was received from the server!");
        }
        checkForErrorResponse(result);
        //response type=application/vnd.org.jfrog.artifactory.storage.FolderInfo+json
        FolderInfo details = result.readEntity(FolderInfo.class);
        closeResponse(result);
        return details;
    }

    @Override
    public StorageInfo getStorageInfo(ArtifactoryData data) throws ArtifactoryClientException {
        WebTarget webTarget = createTarget(data).path("/api/storageinfo");
        Invocation.Builder builder = applyRequestTypes(webTarget);
        Response result = builder.get();
        if (result == null) {
            throw new ArtifactoryClientException(Errors.NO_RESULT, "No result was received from the server!");
        }
        checkForErrorResponse(result);
        StorageInfo details = result.readEntity(StorageInfo.class);
        closeResponse(result);
        return details;
    }

    @Override
    public Collection<RepositoryDetails> getRepositories(ArtifactoryData data, RepositoryType type) throws ArtifactoryClientException {
        WebTarget webTarget = createTarget(data).path("/api/repositories");
        if (type != null) {
            webTarget = webTarget.queryParam("type", type.name().toLowerCase());
        }
        Invocation.Builder builder = applyRequestTypes(webTarget);
        Response result = builder.get();
        if (result == null) {
            throw new ArtifactoryClientException(Errors.NO_RESULT, "No result was received from the server!");
        }
        checkForErrorResponse(result);
        Set<RepositoryDetails> details = result.readEntity(new GenericType<Set<RepositoryDetails>>() {
        });
        closeResponse(result);
        return details;
    }
}
