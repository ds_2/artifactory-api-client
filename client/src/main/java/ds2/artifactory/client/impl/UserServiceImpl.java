package ds2.artifactory.client.impl;

import ds2.artifactory.client.api.ArtifactoryClientException;
import ds2.artifactory.client.api.ArtifactoryData;
import ds2.artifactory.client.api.json.User;
import ds2.artifactory.client.api.json.UserLockPolicy;
import ds2.artifactory.client.api.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.lang.invoke.MethodHandles;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public class UserServiceImpl extends AbstractJaxRsBase implements UserService {
    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Override
    public Collection<User> getUsers(ArtifactoryData data) throws ArtifactoryClientException {
        LOG.debug("Creating url..");
        WebTarget webTarget = createTarget(data).path("/api/security/users");
        Invocation.Builder builder = applyRequestTypes(webTarget);
        LOG.debug("Perform query..");
        Response response = builder.get();
        checkForErrorResponse(response);
        GenericType<List<User>> usersList = new GenericType<List<User>>() {
        };
        List<User> users = response.readEntity(usersList);
        closeResponse(response);
        LOG.debug("Result: {}", users);
        return users;
    }

    @Override
    public void setUserLockPolicy(ArtifactoryData data, boolean enable, int countFailedLoginAttempts) throws ArtifactoryClientException {
        WebTarget webTarget = createTarget(data).path("/api/security/userLockPolicy");
        Invocation.Builder builder = applyRequestTypes(webTarget);
        Response response = builder.put(Entity.json(new UserLockPolicy(enable, countFailedLoginAttempts)));
        checkForErrorResponse(response);
        String result = response.readEntity(String.class);
        closeResponse(response);
        LOG.debug("Result: {}", result);
    }

    @Override
    public UserLockPolicy getUserLockPolicy(ArtifactoryData data) throws ArtifactoryClientException {
        WebTarget webTarget = createTarget(data).path("/api/security/userLockPolicy");
        Invocation.Builder builder = applyRequestTypes(webTarget);
        Response response = builder.get();
        checkForErrorResponse(response);
        UserLockPolicy result = response.readEntity(UserLockPolicy.class);
        closeResponse(response);
        LOG.debug("Result: {}", result);
        return result;
    }

    @Override
    public Set<String> getLockedOutUsers(ArtifactoryData data) throws ArtifactoryClientException {
        WebTarget webTarget = createTarget(data).path("/api/security/lockedUsers");
        Invocation.Builder builder = applyRequestTypes(webTarget);
        Response response = builder.get();
        checkForErrorResponse(response);
        Set<String> result = response.readEntity(new GenericType<Set<String>>() {
        });
        closeResponse(response);
        LOG.debug("Result: {}", result);
        return result;
    }

    @Override
    public void unlockUsers(ArtifactoryData data, String... users) throws ArtifactoryClientException {
        LOG.debug("Preparing unlock url..");
        WebTarget webTarget = createTarget(data).path("/api/security/unlockAllUsers");
        Invocation.Builder builder = applyRequestTypes(webTarget);
        Response response = null;
        LOG.debug("Perform query..");
        if (users == null || users.length <= 0) {
            response = builder.post(null);
        } else {
            LOG.debug("Unlocking multiple users: {}", users);
            response = builder.post(Entity.json(Arrays.asList(users)));
        }
        LOG.debug("done, we have a result, either good or bad..");
        checkForErrorResponse(response);
        closeResponse(response);
    }
}
