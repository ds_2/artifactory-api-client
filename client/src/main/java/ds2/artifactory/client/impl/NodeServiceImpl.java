package ds2.artifactory.client.impl;

import ds2.artifactory.client.api.ArtifactoryClientException;
import ds2.artifactory.client.api.ArtifactoryData;
import ds2.artifactory.client.api.services.NodeService;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

public class NodeServiceImpl extends AbstractJaxRsBase implements NodeService {
    @Override
    public String getServiceId(ArtifactoryData data) throws ArtifactoryClientException {
        WebTarget webTarget = createTarget(data).path("/api/system/service_id");
        Invocation.Builder builder = applyRequestTypes(webTarget);
        Response response = builder.get();
        checkForErrorResponse(response);
        String serviceId = response.readEntity(String.class);
        closeResponse(response);
        return serviceId;
    }

    @Override
    public String getSystemInfo(ArtifactoryData data) throws ArtifactoryClientException {
        WebTarget webTarget = createTarget(data).path("/api/system");
        Invocation.Builder builder = applyRequestTypes(webTarget);
        Response response = builder.get();
        checkForErrorResponse(response);
        String serviceId = response.readEntity(String.class);
        closeResponse(response);
        return serviceId;
    }

    @Override
    public boolean ping(ArtifactoryData data) throws ArtifactoryClientException {
        WebTarget webTarget = createTarget(data).path("/api/system/ping");
        Invocation.Builder builder = applyRequestTypes(webTarget);
        Response response = builder.get();
        checkForErrorResponse(response);
        String okMessage = response.readEntity(String.class);
        closeResponse(response);
        return "OK".equalsIgnoreCase(okMessage);
    }
}
