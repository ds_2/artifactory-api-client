package ds2.artifactory.client.impl;

import ds2.artifactory.client.api.ArtifactoryClientException;
import ds2.artifactory.client.api.ArtifactoryData;
import ds2.artifactory.client.api.Errors;
import ds2.artifactory.client.api.json.ArtifactUsage;
import ds2.artifactory.client.api.json.ArtifactUsageResult;
import ds2.artifactory.client.api.json.GavcSearchResult;
import ds2.artifactory.client.api.json.SearchResult;
import ds2.artifactory.client.api.services.ArtifactService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.lang.invoke.MethodHandles;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import static ds2.artifactory.client.api.Methods.isBlank;
import static ds2.artifactory.client.api.Methods.isEmpty;

public class ArtifactServiceImpl extends AbstractJaxRsBase implements ArtifactService {
    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Override
    public void uploadLocalFile(ArtifactoryData data, Path file, String repoKey, Path remoteDirectory) throws ArtifactoryClientException {
        WebTarget webTarget = createTarget(data).path(repoKey).path(remoteDirectory.normalize().toAbsolutePath().toString());
        Invocation.Builder builder = applyRequestTypes(webTarget);
        try (InputStream is = Files.newInputStream(file, StandardOpenOption.READ)) {
            Response response = builder.put(Entity.entity(is, guessMediaTypeFromFile(file)));
            checkForErrorResponse(response);
        } catch (IOException e) {
            throw new ArtifactoryClientException(Errors.UPLOAD_IO_ERROR, "Error when uploading the file!", e);
        }

    }

    @Override
    public List<SearchResult> searchByGavc(ArtifactoryData data, String groupId, String artifactId, String version, String classifier, Set<String> repositories, boolean withProperties, boolean withInfo) throws ArtifactoryClientException {
        WebTarget webTarget = createTarget(data).path("/api/search/gavc");
        if (!isOnlyWcOrEmpty(groupId)) {
            webTarget = webTarget.queryParam("g", groupId);
        }
        if (!isOnlyWcOrEmpty(artifactId)) {
            webTarget = webTarget.queryParam("a", artifactId);
        }
        if (!isOnlyWcOrEmpty(version)) {
            webTarget = webTarget.queryParam("v", version);
        }
        if (!isOnlyWcOrEmpty(classifier)) {
            webTarget = webTarget.queryParam("c", classifier);
        }
        if (!isEmpty(repositories)) {
            webTarget = webTarget.queryParam("repos", concatStrings(repositories));
        }
        if (isOnlyWcOrEmpty(groupId) && isOnlyWcOrEmpty(artifactId) && isOnlyWcOrEmpty(version)) {
            throw new IllegalArgumentException("You must give at least one of groupId, artifactId or version!");
        }
        Invocation.Builder builder = applyRequestTypes(webTarget);
        if (withInfo && !withProperties) {
            builder.header("X-Result-Detail", "info");
        } else if (withInfo && withProperties) {
            builder.header("X-Result-Detail", "info,properties");
        } else if (!withInfo && withProperties) {
            builder.header("X-Result-Detail", "properties");
        }
        Response response = builder.get();
        checkForErrorResponse(response);
        GavcSearchResult searchResult = response.readEntity(GavcSearchResult.class);
        closeResponse(response);
        return searchResult.getResults();
    }

    @Override
    public List<ArtifactUsage> getArtifactsNotDownloadedSince(ArtifactoryData data, Instant notUsedSince, Instant createdBefore, Set<String> repositories) throws ArtifactoryClientException {
        LOG.debug("Trying to get all files that are not used since {} and createdBefore {}, in repos {}", notUsedSince, createdBefore, repositories);
        WebTarget webTarget = createTarget(data).path("/api/search/usage");
        if (notUsedSince != null) {
            webTarget = webTarget.queryParam("notUsedSince", notUsedSince.toEpochMilli());
        }
        if (createdBefore != null) {
            webTarget = webTarget.queryParam("createdBefore", createdBefore.toEpochMilli());
        }
        if (!isEmpty(repositories)) {
            webTarget = webTarget.queryParam("repos", concatStrings(repositories));
        }
        Invocation.Builder builder = applyRequestTypes(webTarget);
        Response response = builder.get();
        checkForErrorResponse(response);
        ArtifactUsageResult searchResult = response.readEntity(ArtifactUsageResult.class);
        closeResponse(response);
        LOG.debug("Returning results: {}", searchResult);
        return searchResult.getResults();
    }

    @Override
    public void deleteFileOrFolder(ArtifactoryData data, String repoKey, String pathToFileOrFolder) throws ArtifactoryClientException {
        LOG.debug("Will try to delete file {} from repo {}", pathToFileOrFolder, repoKey);
        if (isOnlyWcOrEmpty(pathToFileOrFolder)) {
            throw new ArtifactoryClientException(Errors.BAD_PARAMS, "This path is not allowed!");
        }
        WebTarget webTarget = createTarget(data).path(repoKey).path(pathToFileOrFolder);
        Invocation.Builder builder = applyRequestTypes(webTarget);
        Response response = builder.delete();
        checkForErrorResponse(response);
    }

    private boolean isOnlyWcOrEmpty(String s) {
        return isBlank(s) || s.equalsIgnoreCase("*");
    }

    private String concatStrings(Collection<String> repositories) {
        StringBuilder sb = new StringBuilder();
        boolean isFirst = true;
        for (String repo : repositories) {
            if (!isFirst) {
                sb.append(",");
            }
            sb.append(repo);
            isFirst = false;
        }
        return sb.toString();
    }

    private MediaType guessMediaTypeFromFile(Path file) throws ArtifactoryClientException {
        String fileName = file.getFileName().toString().toLowerCase();
        MediaType mediaType = null;
        try {
            String possibleType = Files.probeContentType(file);
            if (possibleType != null && possibleType.trim().length() > 0) {
                mediaType = MediaType.valueOf(possibleType);
            }
        } catch (IOException e) {
            throw new ArtifactoryClientException(Errors.UPLOAD_IO_ERROR, "Could not read file " + file, e);
        }
        if (mediaType == null) {
            throw new ArtifactoryClientException(Errors.UNKNOWN_FILETYPE, "Could not get media type from file " + fileName);
        }
        return mediaType;
    }
}
