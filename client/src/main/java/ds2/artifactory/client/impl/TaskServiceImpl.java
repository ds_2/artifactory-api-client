package ds2.artifactory.client.impl;

import ds2.artifactory.client.api.ArtifactoryClientException;
import ds2.artifactory.client.api.ArtifactoryData;
import ds2.artifactory.client.api.Errors;
import ds2.artifactory.client.api.json.Task;
import ds2.artifactory.client.api.services.TaskService;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.util.Collection;
import java.util.List;

public class TaskServiceImpl extends AbstractJaxRsBase implements TaskService {
    @Override
    public Collection<Task> getBackgroundTasks(ArtifactoryData data) throws ArtifactoryClientException {
        WebTarget webTarget = createTarget(data).path("/api/tasks");
        Invocation.Builder builder = applyRequestTypes(webTarget);
        Response result = builder.get();
        if (result == null) {
            throw new ArtifactoryClientException(Errors.NO_RESULT, "No result was received from the server!");
        }
        checkForErrorResponse(result);
        TaskResult details = result.readEntity(TaskResult.class);
        closeResponse(result);
        return details.getTasks();
    }

    @Getter
    @Setter
    @ToString
    private class TaskResult {
        private List<Task> tasks;
    }
}
