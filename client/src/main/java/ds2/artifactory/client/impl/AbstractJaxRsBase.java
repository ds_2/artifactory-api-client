package ds2.artifactory.client.impl;

import ds2.artifactory.client.api.ArtifactoryClientException;
import ds2.artifactory.client.api.ArtifactoryData;
import ds2.artifactory.client.api.Errors;
import ds2.artifactory.client.api.json.Error;
import ds2.artifactory.client.api.json.SimpleError;
import ds2.artifactory.client.impl.filters.BasicAuthFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.lang.invoke.MethodHandles;
import java.net.URISyntaxException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class AbstractJaxRsBase {
    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private Client client;
    private static final Lock LOCK = new ReentrantLock();

    private Client createClient(ArtifactoryData data) {
        LOG.debug("Getting current client..");
        LOCK.lock();
        try {
            if (client == null) {
                LOG.debug("Will need to create a new client..");
                ClientBuilder builder = ClientBuilder.newBuilder();
                client = builder.build();
                client.register(new BasicAuthFilter(data));
                client.register(new MyPatchedGsonProvider<>());
            }
        } finally {
            LOCK.unlock();
        }
        LOG.debug("ok, returning client");
        return client;
    }

    protected WebTarget createTarget(ArtifactoryData data) throws ArtifactoryClientException {
        try {
            WebTarget webTarget = createClient(data).target(data.getServerUrl().toURI());
            return webTarget;
        } catch (URISyntaxException e) {
            throw new ArtifactoryClientException(Errors.BAD_URI, "Error when converting the URL of the server!", e);
        }
    }

    protected Invocation.Builder applyRequestTypes(WebTarget webTarget) {
        return webTarget.request(MediaType.APPLICATION_JSON_TYPE, MediaType.APPLICATION_XML_TYPE, MediaType.TEXT_XML_TYPE, MediaType.TEXT_PLAIN_TYPE);
    }

    protected void checkForErrorResponse(Response response) throws ArtifactoryClientException {
        int status = response.getStatus();
        LOG.debug("Status response code was: {}", status);
        LOG.debug("Headers so far from the error: {}", response.getHeaders());
        if (status >= 400) {
            MediaType thisMediaType = response.getMediaType();
            if (thisMediaType.isCompatible(MediaType.APPLICATION_JSON_TYPE)) {
                Error result = response.readEntity(Error.class);
                if (result != null) {
                    closeResponse(response);
                    for (SimpleError error : result.getErrors()) {
                        LOG.debug("This error: {}", error);
                        if ("no results found.".equalsIgnoreCase(error.getMessage()) && error.getStatus() == 404) {
                            throw new ArtifactoryClientException(Errors.NO_RESULT, "No results found!");
                        } else if ("Unauthorized".equalsIgnoreCase(error.getMessage()) && error.getStatus() == 401) {
                            throw new ArtifactoryClientException(Errors.UNAUTHORIZED, "You are not authorized!");
                        }
                        switch (status) {
                            case 403:
                                throw new ArtifactoryClientException(Errors.SERVER_FORBIDDEN, "This operation is forbidden!");
                            default:
                                throw new ArtifactoryClientException(Errors.SERVER_ERROR, "Error from the server: " + error.getMessage());
                        }
                    }
                }
            }
            String responseBody = response.readEntity(String.class);
            LOG.debug("Response body: {}", responseBody);
            closeResponse(response);
            switch (status) {
                case 403:
                    throw new ArtifactoryClientException(Errors.SERVER_FORBIDDEN, "This operation is forbidden!");
                default:
                    throw new ArtifactoryClientException(Errors.SERVER_ERROR, "Error from the server: " + status);
            }

        }
    }

    protected void closeResponse(Response response) {
        LOG.debug("Closing response");
        try {
            response.close();
        } catch (ProcessingException e) {
            LOG.debug("Error when closing the response, but is ignored!", e);
        } finally {
            LOG.debug("Done closing response.");
        }
    }
}
