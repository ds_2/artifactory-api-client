package ds2.artifactory.client.impl;

import ds2.artifactory.client.api.ArtifactoryClientException;
import ds2.artifactory.client.api.ArtifactoryData;
import ds2.artifactory.client.api.json.ApiKey;
import ds2.artifactory.client.api.services.AuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.lang.invoke.MethodHandles;

public class AuthenticationServiceImpl extends AbstractJaxRsBase implements AuthenticationService {
    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Override
    public void performLogin(ArtifactoryData data) throws ArtifactoryClientException {
        //nothing to do
    }

    @Override
    public void logout(ArtifactoryData data) throws ArtifactoryClientException {
        //nothing to do
    }

    @Override
    public String getApiKey(ArtifactoryData data) throws ArtifactoryClientException {
        WebTarget webTarget = createTarget(data).path("/api/security/apiKey");
        Invocation.Builder builder = applyRequestTypes(webTarget);
        Response response = builder.get();
        checkForErrorResponse(response);
        ApiKey apiKey = response.readEntity(ApiKey.class);
        closeResponse(response);
        LOG.debug("api key: {}", apiKey);
        return apiKey != null ? apiKey.getApiKey() : null;
    }

    @Override
    public String generateApiKey(ArtifactoryData data) throws ArtifactoryClientException {
        WebTarget webTarget = createTarget(data).path("/api/security/apiKey");
        Invocation.Builder builder = applyRequestTypes(webTarget);
        Response response = builder.post(null);
        checkForErrorResponse(response);
        ApiKey apiKey = response.readEntity(ApiKey.class);
        closeResponse(response);
        return apiKey.getApiKey();
    }

    @Override
    public String regenerateApiKey(ArtifactoryData data) throws ArtifactoryClientException {
        WebTarget webTarget = createTarget(data).path("/api/security/apiKey");
        Invocation.Builder builder = applyRequestTypes(webTarget);
        Response response = builder.put(null);
        checkForErrorResponse(response);
        ApiKey apiKey = response.readEntity(ApiKey.class);
        closeResponse(response);
        return apiKey.getApiKey();
    }

    @Override
    public void removeApiKeyFromUser(ArtifactoryData data, String username, boolean deleteAllKeys) throws ArtifactoryClientException {
        WebTarget webTarget = createTarget(data).path("/api/security/apiKey");
        if (username != null && username.trim().length() > 0) {
            webTarget = webTarget.path(username);
        }
        webTarget = webTarget.queryParam("deleteAll", deleteAllKeys ? "1" : "0");
        Invocation.Builder builder = applyRequestTypes(webTarget);
        Response response = builder.delete();
        checkForErrorResponse(response);
        closeResponse(response);
    }
}
