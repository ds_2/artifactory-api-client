package ds2.artifactory.client.impl.filters;

import ds2.artifactory.client.api.ArtifactoryData;

import javax.annotation.Priority;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

@Provider
@Priority(1000)
public class BasicAuthFilter implements ClientRequestFilter {
    private ArtifactoryData data;

    public BasicAuthFilter(ArtifactoryData data) {
        this.data = data;
    }

    @Override
    public void filter(ClientRequestContext requestContext) throws IOException {
        if (data.getApiKey() != null) {
            requestContext.getHeaders().putSingle("X-JFrog-Art-Api", String.copyValueOf(data.getApiKey()));
        } else if (data.getUserPassword() != null) {
            String base64Data = data.getUserName() + ":" + String.copyValueOf(data.getUserPassword());
            String authHeader = "Basic " + Base64.getEncoder().encodeToString(base64Data.getBytes(StandardCharsets.UTF_8));
            requestContext.getHeaders().putSingle(HttpHeaders.AUTHORIZATION, authHeader);
        }
    }
}
