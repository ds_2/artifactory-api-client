# DS/2 Artifactory API Client

[ ![Download](https://api.bintray.com/packages/dstrauss/ds2-oss/ds2-artifactory-client/images/download.svg) ](https://bintray.com/dstrauss/ds2-oss/ds2-artifactory-client/_latestVersion)

An unofficial client to access an artifactory server using its remote api.

## Build it

Via Gradle:

    ./gradlew clean build shadowJar publishToMavenLocal

## Perform release

Switch to the current release branch. Then merge the needed changes from the develop branch into this release branch.

Then, release via:

    ./gradlew clean release

This will create the tag. Now, to publish it, you need to checkout the release tag and perform the task "bintrayUpload".
For some reason, you cannot chain the release cycle with it on multi-module projects.
Your environment must contain the BINTRAY_USER and BINTRAY_API_KEY entries in order to allow to push
to Bintray.

    git checkout VERSIONTAG

Now upload it:

    ./gradlew bintrayUpload

## The Jersey Json Gson jar

You can find the project here: <https://gitlab.com/ds_2/jersey-media-json-gson>

Checkout the project, checkout the tag and build it ;)
