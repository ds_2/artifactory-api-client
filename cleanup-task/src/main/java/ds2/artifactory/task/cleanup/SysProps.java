package ds2.artifactory.task.cleanup;

public interface SysProps {
    String TSK_URL="task.url";
    String TSK_USERNAME="task.user";
    String TSK_PW="task.pw";
    String TSK_APIKEY="task.apikey";
    String TSK_DAYS="task.days";
    String TSK_REPO="task.repo";
}
