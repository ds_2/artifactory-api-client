package ds2.artifactory.task.cleanup;

import ds2.artifactory.client.api.ArtifactoryClientException;
import ds2.artifactory.client.api.Errors;
import ds2.artifactory.client.api.Methods;
import ds2.artifactory.client.api.dto.ArtifactoryDataDto;
import ds2.artifactory.client.api.json.ArtifactUsage;
import ds2.artifactory.client.api.services.ArtifactService;
import ds2.artifactory.client.impl.ArtifactServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static ds2.artifactory.client.api.Methods.isBlank;

public class CleanTask implements SysProps {
    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public static void main(String[] args) {
        LOG.info("Starting task..");
        if (containsHelpParam(args)) {
            printHelp();
            return;
        }
        ArtifactoryDataDto dataDto = new ArtifactoryDataDto();
        try {
            dataDto.setServerUrl(new URL(System.getProperty(TSK_URL, "http://localhost:8081/artifactory")));
            if (System.getProperty(TSK_PW) != null) {
                dataDto.setUserPassword(System.getProperty(TSK_PW).toCharArray());
            }
            if (System.getProperty(TSK_USERNAME) != null) {
                dataDto.setUserName(System.getProperty(TSK_USERNAME));
            }
            if (System.getProperty(TSK_APIKEY) != null) {
                dataDto.setApiKey(System.getProperty(TSK_APIKEY).toCharArray());
            }
            Long minusDays = Long.getLong(TSK_DAYS, 150L);
            if (minusDays == null || minusDays.longValue() <= 0) {
                throw new ArtifactoryClientException(Errors.BAD_PARAMS, "The value for the days is too small: " + minusDays);
            }
            String repository = System.getProperty(TSK_REPO);
            LOG.info("Using config {}", dataDto);
            ArtifactService artifactService = new ArtifactServiceImpl();
            List<ArtifactUsage> foundArtifacts = artifactService.getArtifactsNotDownloadedSince(dataDto, Instant.now().minus(Duration.ofDays(minusDays)), null, asSet(repository));
            long deletionCount = 0;
            if (!Methods.isEmpty(foundArtifacts)) {
                for (ArtifactUsage usage : foundArtifacts) {
                    LOG.debug("Checking usage {}", usage);
                    String path = extractPath(repository, usage.getUri());
                    if (path == null || path.trim().length() <= 2) {
                        continue;
                    }
                    LOG.info("Will delete file {}", path);
                    artifactService.deleteFileOrFolder(dataDto, repository, path);
                    LOG.debug("done with deletion marker.");
                    ++deletionCount;
                }
                LOG.info("{} files have been marked for deletion!", deletionCount);
            } else {
                LOG.info("Nothing found. Ignoring.");
            }
        } catch (MalformedURLException e) {
            LOG.error("Bad URL!", e);
        } catch (ArtifactoryClientException e) {
            LOG.error("Error when executing the query!", e);
        }
        LOG.info("Done");
    }

    private static void printHelp() {
        System.out.println("Help:");
        System.out.println("java [Options] -jar THISJAR.jar");
        System.out.println("Options:");
        System.out.println("\t-D" + TSK_APIKEY + "=MYAPIKEY = sets the api key to use for authentication");
        System.out.println("\t-D" + TSK_USERNAME + "=MYUSER = sets the username for authentication");
        System.out.println("\t-D" + TSK_PW + "=MYPW = sets the password for basic authentication");
        System.out.println("\t-D" + TSK_URL + "=http://www.bla.test/artifactory = sets the url to the artifactory instance");
        System.out.println("\t-D" + TSK_REPO + "=releases = sets the repository to use. If omitted, all repositories are used!");
        System.out.println("\t-D" + TSK_DAYS + "=100 = sets the retention days for an artifact. All older artifacts are considered to be deleted.");
    }

    private static boolean containsHelpParam(String[] args) {
        if (args != null) {
            for (String arg : args) {
                if ("-h".equalsIgnoreCase(arg)) {
                    return true;
                } else if ("--h".equalsIgnoreCase(arg)) {
                    return true;
                } else if (arg.startsWith("--h")) {
                    return true;
                }
            }
        }
        return false;
    }

    private static Set<String> asSet(String repository) {
        if (isBlank(repository)) {
            return null;
        }
        Set<String> strings = new HashSet<>(1);
        strings.add(repository);
        return strings;
    }

    private static String extractPath(String repository, String uri) {
        int indexOf = uri.indexOf(repository);
        if (indexOf <= 0) {
            LOG.warn("Could not detect path uri in {} using repository {}.", uri, repository);
            return null;
        }

        return uri.substring(indexOf + repository.length());
    }

}
