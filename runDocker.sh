#!/bin/bash

CONTAINERNAME="artifactory"

function init(){
    docker pull docker.bintray.io/jfrog/artifactory-oss:latest
    docker volume create --name artifactory5_data
}

function run(){
    docker run --name $CONTAINERNAME -d -v artifactory5_data:/var/opt/jfrog/artifactory -p 12345:8081 -e EXTRA_JAVA_OPTIONS=-Xmx2g docker.bintray.io/jfrog/artifactory-oss:latest
}

function log(){
    docker logs -f -t $CONTAINERNAME
}

function enterContainer(){
    docker exec -it $CONTAINERNAME /bin/bash
}

function removeOldContainer(){
    docker rm $CONTAINERNAME
}

case $1 in
init)
    init
;;
run)
    run
;;
stop)
    docker stop $CONTAINERNAME
;;
log)
    log
;;
enter)
    enterContainer
;;
remove)
    removeOldContainer
;;
*)
    echo "$0 (init|run|stop|log|enter)"
    echo "   init = downloads the docker image"
    echo "   run = runs the docker image"
    echo "   stop = stops the docker image"
    echo "   log = opens the docker log file for this container"
    echo "   enter = enters the running container via bash"
;;
esac
