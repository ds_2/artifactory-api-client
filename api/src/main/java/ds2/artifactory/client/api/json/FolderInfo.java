package ds2.artifactory.client.api.json;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.net.URI;
import java.time.Instant;
import java.util.Date;
import java.util.List;

/**
 * application/vnd.org.jfrog.artifactory.storage.FolderInfo+json
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class FolderInfo {
    private URI uri;
    private String repo;
    private String path;
    private Date created;
    private String createdBy;
    private Date lastModified;
    private String modifiedBy;
    private Date lastUpdated;
    private List<FolderInfoChildren> children;
}
