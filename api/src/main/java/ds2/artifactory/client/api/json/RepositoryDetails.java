package ds2.artifactory.client.api.json;

import lombok.*;

import java.net.URL;

@Setter
@Getter
@ToString
@NoArgsConstructor
@EqualsAndHashCode(of = {"key", "type"})
public class RepositoryDetails {
    /*
    {
         "key" : "libs-releases-local",
         "type" : "LOCAL",
         "description" : "Local repository for in-house libraries",
         "url" : "http://localhost:8081/artifactory/libs-releases-local"
       }
     */
    private String key;
    private String type;
    private String description;
    private URL url;

    public RepositoryType asRepositoryType() {
        return RepositoryType.valueOf(type);
    }
}
