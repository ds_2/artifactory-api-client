package ds2.artifactory.client.api;

import java.util.Collection;

public interface Methods {
    static boolean isBlank(String s) {
        boolean blank = true;
        if (s != null && s.trim().length() > 0) {
            blank = false;
        }
        return blank;
    }

    static boolean isEmpty(Collection<?> collection) {
        boolean empty = true;
        if (collection != null && !collection.isEmpty()) {
            empty = false;
        }
        return empty;
    }
}
