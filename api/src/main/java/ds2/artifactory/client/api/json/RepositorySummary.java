package ds2.artifactory.client.api.json;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@EqualsAndHashCode(of = {"repoKey"})
public class RepositorySummary {
    /*
    "repoKey": "plugins-release",
      "repoType": "VIRTUAL",
      "foldersCount": 0,
      "filesCount": 0,
      "usedSpace": "0 bytes",
      "itemsCount": 0,
      "packageType": "Maven",
      "percentage": "0%"
     */
    private String repoKey;
    private String repoType;
    private Long foldersCount;
    private Long itemsCount;
    private Long filesCount;
    private String usedSpace;
    private String packageType;
    private String percentage;

    public RepositoryType asRepositoryType() {
        return RepositoryType.valueOf(repoType);
    }

    public PackageType asPackageType() {
        return PackageType.valueOf(packageType);
    }
}
