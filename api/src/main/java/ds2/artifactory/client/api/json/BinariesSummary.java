package ds2.artifactory.client.api.json;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class BinariesSummary {
    /*
    "binariesCount": "125,726",
    "binariesSize": "3.48 GB",
    "artifactsSize": "59.77 GB",
    "optimization": "5.82%",
    "itemsCount": "2,176,580",
    "artifactsCount": "2,084,408"
     */
    private String binariesCount;
    private String binariesSize;
    private String artifactsSize;
    private String optimization;
    private String itemsCount;
    private String artifactsCount;
}
