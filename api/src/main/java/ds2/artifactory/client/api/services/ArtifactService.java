package ds2.artifactory.client.api.services;

import ds2.artifactory.client.api.ArtifactoryClientException;
import ds2.artifactory.client.api.ArtifactoryData;
import ds2.artifactory.client.api.json.ArtifactUsage;
import ds2.artifactory.client.api.json.SearchResult;

import java.net.URI;
import java.nio.file.Path;
import java.time.Instant;
import java.util.List;
import java.util.Set;

public interface ArtifactService {
    void uploadLocalFile(ArtifactoryData data, Path file, String repoKey, Path remoteDirectory) throws ArtifactoryClientException;

    List<SearchResult> searchByGavc(ArtifactoryData data, String groupId, String artifactId, String version, String classifier, Set<String> repositories, boolean withProperties, boolean withInfo) throws ArtifactoryClientException;

    List<ArtifactUsage> getArtifactsNotDownloadedSince(ArtifactoryData data, Instant notUsedSince, Instant createdBefore, Set<String> repositories) throws ArtifactoryClientException;

    void deleteFileOrFolder(ArtifactoryData data, String repository, String pathToFileOrFolder) throws ArtifactoryClientException;
}
