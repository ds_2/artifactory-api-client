package ds2.artifactory.client.api.json;

public enum RepositoryType {
    /**
     * Virtual. Say, a proxy repository.
     */
    VIRTUAL,
    /**
     * Not Available.
     */
    NA,
    LOCAL,
    CACHE,
    DISTRIBUTION,
    REMOTE;
}
