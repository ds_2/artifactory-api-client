package ds2.artifactory.client.api.services;

import ds2.artifactory.client.api.ArtifactoryClientException;
import ds2.artifactory.client.api.ArtifactoryData;

public interface AuthenticationService {
    /**
     * Performs a login of the given user, replacing any currently used authentication information within the service.
     */
    void performLogin(ArtifactoryData data) throws ArtifactoryClientException;

    void logout(ArtifactoryData data) throws ArtifactoryClientException;

    String getApiKey(ArtifactoryData data) throws ArtifactoryClientException;

    String generateApiKey(ArtifactoryData data) throws ArtifactoryClientException;

    String regenerateApiKey(ArtifactoryData data) throws ArtifactoryClientException;

    void removeApiKeyFromUser(ArtifactoryData data, String username, boolean deleteAllKeys) throws ArtifactoryClientException;
}
