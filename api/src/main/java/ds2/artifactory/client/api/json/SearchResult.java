package ds2.artifactory.client.api.json;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

@Setter
@Getter
@ToString
@EqualsAndHashCode(of = "uri")
public class SearchResult {
    /*
    {
    "repo" : "jcenter-cache",
    "path" : "/org/jruby/extras/bytelist/1.0.13/bytelist-1.0.13.pom",
    "created" : "2017-12-19T12:18:39.089Z",
    "createdBy" : "tignum",
    "lastModified" : "2015-04-22T16:47:00.000Z",
    "modifiedBy" : "tignum",
    "lastUpdated" : "2017-12-19T12:18:39.093Z",
    "properties" : {
      "artifactory.internal.etag" : [ "d44413453d2f235c074e7f15b21fce15097c2870" ]
    },
    "downloadUri" : "http://artifacts.tignum.com/artifactory/jcenter-cache/org/jruby/extras/bytelist/1.0.13/bytelist-1.0.13.pom",
    "remoteUrl" : "https://jcenter.bintray.com/org/jruby/extras/bytelist/1.0.13/bytelist-1.0.13.pom",
    "mimeType" : "application/x-maven-pom+xml",
    "size" : "3037",
    "checksums" : {
      "sha1" : "d44413453d2f235c074e7f15b21fce15097c2870",
      "md5" : "81fa72ff63dc94816bd8fc42608d1e70",
      "sha256" : "bab964138ce0690a8d55a29e8c13c307b18f7dbc43e94289e9d13b892d12afeb"
    },
    "originalChecksums" : {
      "sha1" : "d44413453d2f235c074e7f15b21fce15097c2870",
      "sha256" : "bab964138ce0690a8d55a29e8c13c307b18f7dbc43e94289e9d13b892d12afeb"
    },
    "uri" : "http://artifacts.tignum.com/artifactory/api/storage/jcenter-cache/org/jruby/extras/bytelist/1.0.13/bytelist-1.0.13.pom"
  }
     */
    private String uri;
    private String repo;
    private String path;
    private Date created;
    private String createdBy;
    private Date lastModified;
    private String modifiedBy;
    private Map<String,List<String>> properties;
    private URL downloadUri;
    private URL remoteUrl;
    private String mimeType;
    private Long size;
    private Properties checksums;
    private Properties originalChecksums;
}
