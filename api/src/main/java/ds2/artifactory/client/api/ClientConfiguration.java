package ds2.artifactory.client.api;

import java.net.URL;

public interface ClientConfiguration {
    URL getServerUrl();
    String getUsername();
    char[] getApiKey();
}
