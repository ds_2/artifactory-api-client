package ds2.artifactory.client.api.json;

import lombok.*;

import java.util.Date;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"uri"})
public class ArtifactUsage {
    private String uri;
    private Date lastDownloaded;
}
