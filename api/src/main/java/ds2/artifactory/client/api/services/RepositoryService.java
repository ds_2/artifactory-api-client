package ds2.artifactory.client.api.services;

import ds2.artifactory.client.api.ArtifactoryClientException;
import ds2.artifactory.client.api.ArtifactoryData;
import ds2.artifactory.client.api.json.FolderInfo;
import ds2.artifactory.client.api.json.RepositoryDetails;
import ds2.artifactory.client.api.json.RepositoryType;
import ds2.artifactory.client.api.json.StorageInfo;

import java.nio.file.Path;
import java.util.Collection;

public interface RepositoryService {
    /**
     * Empties the current trash can. (this will NOT perform any storage cleanup)
     */
    void emptyTrashcan(ArtifactoryData data) throws ArtifactoryClientException;

    void calculateMavenMetadata(ArtifactoryData data, String repositoryName, Path folderPath, Boolean recursive) throws ArtifactoryClientException;

    FolderInfo getFolderInfo(ArtifactoryData data, String repositoryName, Path folderPath) throws ArtifactoryClientException;

    StorageInfo getStorageInfo(ArtifactoryData data) throws ArtifactoryClientException;

    Collection<RepositoryDetails> getRepositories(ArtifactoryData data, RepositoryType type) throws ArtifactoryClientException;
}
