package ds2.artifactory.client.api;

import lombok.Getter;

@Getter
public class ArtifactoryClientException extends Exception {
    private Errors error;

    public ArtifactoryClientException(Errors error, String message) {
        super(message);
        this.error = error;
    }

    public ArtifactoryClientException(Errors error, String message, Throwable cause) {
        super(message, cause);
        this.error = error;
    }
}
