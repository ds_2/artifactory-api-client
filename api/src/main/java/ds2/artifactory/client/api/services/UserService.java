package ds2.artifactory.client.api.services;

import ds2.artifactory.client.api.ArtifactoryClientException;
import ds2.artifactory.client.api.ArtifactoryData;
import ds2.artifactory.client.api.annotations.RequiresProLicense;
import ds2.artifactory.client.api.json.User;
import ds2.artifactory.client.api.json.UserLockPolicy;

import java.util.Collection;
import java.util.Set;

public interface UserService {
    @RequiresProLicense
    Collection<User> getUsers(ArtifactoryData data) throws ArtifactoryClientException;

    void setUserLockPolicy(ArtifactoryData data, boolean enable, int countFailedLoginAttempts) throws ArtifactoryClientException;

    UserLockPolicy getUserLockPolicy(ArtifactoryData data) throws ArtifactoryClientException;

    Set<String> getLockedOutUsers(ArtifactoryData data) throws ArtifactoryClientException;

    void unlockUsers(ArtifactoryData data, String... users) throws ArtifactoryClientException;
}
