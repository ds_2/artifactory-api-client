package ds2.artifactory.client.api.json;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.net.URI;

@Setter
@Getter
@ToString
@EqualsAndHashCode
public class FolderInfoChildren {
    private URI uri;
    private boolean folder;
}
