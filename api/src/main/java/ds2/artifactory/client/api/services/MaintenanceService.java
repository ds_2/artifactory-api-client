package ds2.artifactory.client.api.services;

import ds2.artifactory.client.api.ArtifactoryClientException;
import ds2.artifactory.client.api.ArtifactoryData;

public interface MaintenanceService {
    /**
     *
     */
    void startGarbageCollection(ArtifactoryData data) throws ArtifactoryClientException;

    void optimizeSystemStorage(ArtifactoryData data) throws ArtifactoryClientException;
}
