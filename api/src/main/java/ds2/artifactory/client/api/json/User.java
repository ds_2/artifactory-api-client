package ds2.artifactory.client.api.json;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.net.URI;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class User {
    private String name;
    private URI uri;
    private String realm;
}
