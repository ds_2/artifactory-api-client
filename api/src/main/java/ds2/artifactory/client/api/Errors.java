package ds2.artifactory.client.api;

import lombok.Getter;

@Getter
public enum Errors {
    BAD_URI(1), NO_RESULT(2), SERVER_ERROR(3), SERVER_FORBIDDEN(4), BAD_PARAMS(5), UPLOAD_IO_ERROR(6), UNKNOWN_FILETYPE(7), UNAUTHORIZED(8);
    private int errorId;

    Errors(int errorId) {
        this.errorId = errorId;
    }

}
