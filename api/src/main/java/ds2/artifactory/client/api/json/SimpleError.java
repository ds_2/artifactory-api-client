package ds2.artifactory.client.api.json;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SimpleError {
    private int status;
    private String message;
}
