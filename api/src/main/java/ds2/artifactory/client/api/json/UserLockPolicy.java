package ds2.artifactory.client.api.json;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UserLockPolicy {
    /*
    {
   "enabled" : true|false,
   "loginAttempts" : {value}
}
     */
    private boolean enabled;
    private int loginAttempts;

}
