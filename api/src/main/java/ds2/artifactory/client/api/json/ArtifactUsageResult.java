package ds2.artifactory.client.api.json;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class ArtifactUsageResult {
    private List<ArtifactUsage> results;
}
