package ds2.artifactory.client.api.services;

import ds2.artifactory.client.api.ArtifactoryClientException;
import ds2.artifactory.client.api.ArtifactoryData;

public interface NodeService {
    String getServiceId(ArtifactoryData data) throws ArtifactoryClientException;

    String getSystemInfo(ArtifactoryData data) throws ArtifactoryClientException;

    boolean ping(ArtifactoryData data) throws ArtifactoryClientException;
}
