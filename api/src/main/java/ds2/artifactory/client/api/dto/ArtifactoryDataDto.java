package ds2.artifactory.client.api.dto;

import ds2.artifactory.client.api.ArtifactoryData;
import lombok.*;

import java.net.URL;

@Getter
@Setter
@ToString(exclude = {"userPassword", "apiKey"})
@NoArgsConstructor
@EqualsAndHashCode(of = {"serverUrl", "userName"})
public class ArtifactoryDataDto implements ArtifactoryData {
    private URL serverUrl;
    private String userName;
    private char[] userPassword;
    private char[] apiKey;
}
