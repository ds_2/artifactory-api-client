package ds2.artifactory.client.api.services;

import ds2.artifactory.client.api.ArtifactoryClientException;
import ds2.artifactory.client.api.ArtifactoryData;
import ds2.artifactory.client.api.json.Task;

import java.util.Collection;

public interface TaskService {
    Collection<Task> getBackgroundTasks(ArtifactoryData data) throws ArtifactoryClientException;
}
