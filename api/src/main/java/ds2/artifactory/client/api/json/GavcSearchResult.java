package ds2.artifactory.client.api.json;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * application/vnd.org.jfrog.artifactory.search.GavcSearchResult+json.
 */
@Setter
@Getter
@ToString
public class GavcSearchResult {
    private List<SearchResult> results;
}
