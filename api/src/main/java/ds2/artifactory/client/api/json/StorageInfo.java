package ds2.artifactory.client.api.json;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class StorageInfo {
    private BinariesSummary binariesSummary;
    private FileStoreSummary fileStoreSummary;
    private List<RepositorySummary> repositoriesSummaryList;
}
