package ds2.artifactory.client.api.json;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class FileStoreSummary {
    /*
    "storageType": "filesystem",
    "storageDirectory": "/home/.../artifactory/devenv/.artifactory/data/filestore",
    "totalSpace": "204.28 GB",
    "usedSpace": "32.22 GB (15.77%)",
    "freeSpace": "172.06 GB (84.23%)"
     */
    private String storageType;
    private String storageDirectory;
    private String totalSpace;
    private String usedSpace;
    private String freeSpace;
}
