package ds2.artifactory.client.api.services;

public interface ArtifactoryService extends AuthenticationService, RepositoryService, MaintenanceService {
    
}
