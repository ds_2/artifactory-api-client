package ds2.artifactory.client.api.json;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Error {
    private List<SimpleError> errors;
}
