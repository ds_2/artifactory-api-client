package ds2.artifactory.client.api;

import java.net.URL;

public interface ArtifactoryData {
    URL getServerUrl();

    String getUserName();

    char[] getUserPassword();

    char[] getApiKey();
}
