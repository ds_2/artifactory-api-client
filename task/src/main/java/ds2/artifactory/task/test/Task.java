package ds2.artifactory.task.test;

import ds2.artifactory.client.api.ArtifactoryClientException;
import ds2.artifactory.client.api.ArtifactoryData;
import ds2.artifactory.client.api.Errors;
import ds2.artifactory.client.api.dto.ArtifactoryDataDto;
import ds2.artifactory.client.api.json.*;
import ds2.artifactory.client.api.services.*;
import ds2.artifactory.client.impl.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public class Task {
    private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public static void main(String[] params) {
        LOG.info("Starting service");
        LOG.debug("Init service..");
        UserService userService = new UserServiceImpl();
        RepositoryService repositoryService = new RepositoryServiceImpl();
        TaskService taskService = new TaskServiceImpl();
        AuthenticationService authenticationService = new AuthenticationServiceImpl();
        NodeService nodeService = new NodeServiceImpl();
        ArtifactService artifactService = new ArtifactServiceImpl();
        ArtifactoryDataDto artifactoryData = new ArtifactoryDataDto();
        try {
            if (params.length < 3) {
                throw new ArtifactoryClientException(Errors.BAD_PARAMS, "The parameter count is too less!");
            }
            artifactoryData.setServerUrl(new URL(params[0]));
            artifactoryData.setUserName(params[1]);
            artifactoryData.setUserPassword(params[2].toCharArray());
            LOG.info("Performing calls..");

            performNodeCalls(artifactoryData, nodeService);

            performArtifactCalls(artifactoryData, artifactService);

            String myApiKey = authenticationService.getApiKey(artifactoryData);
            LOG.info("My api key: {}", myApiKey);

            Collection<ds2.artifactory.client.api.json.Task> tasks = taskService.getBackgroundTasks(artifactoryData);
            LOG.info("Tasks running: {}", tasks);

            performRepoCalls(artifactoryData, repositoryService);

            performUserCalls(artifactoryData, userService);

        } catch (MalformedURLException e) {
            LOG.error("Error when converting the url!", e);
            System.exit(1);
        } catch (ArtifactoryClientException e) {
            LOG.error("Error when executing the query!", e);
            System.exit(2);
        }
        LOG.info("Done with task");
    }

    private static void performArtifactCalls(ArtifactoryDataDto artifactoryData, ArtifactService artifactService) {
        try {
            List<SearchResult> result = artifactService.searchByGavc(artifactoryData, "org.*", null, null, null, null, true, true);
            LOG.info("Result gavc search: {}", result);

            List<ArtifactUsage> oldArtifacts = artifactService.getArtifactsNotDownloadedSince(artifactoryData, Instant.now().minus(Duration.ofDays(100)), null, null);
            LOG.info("Result oldArtifacts search: {}", oldArtifacts);
        } catch (ArtifactoryClientException e) {
            LOG.error("Error when performing some artifact service calls!", e);
        } finally {

        }
    }

    private static void performUserCalls(ArtifactoryDataDto artifactoryData, UserService userService) {
        UserLockPolicy userLockPolicy = null;
        try {
            userLockPolicy = userService.getUserLockPolicy(artifactoryData);
            LOG.info("UserLockPolicy: {}", userLockPolicy);

            Set<String> lockedUsers = userService.getLockedOutUsers(artifactoryData);
            LOG.info("Locked Users: {}", lockedUsers);
        } catch (ArtifactoryClientException e) {
            LOG.error("Error when performing the user method!", e);
        }
    }

    private static void performRepoCalls(ArtifactoryData data, RepositoryService repositoryService) {
        try {
            Collection<RepositoryDetails> repositories = repositoryService.getRepositories(data, null);
            LOG.info("Repositories: {}", repositories);

            StorageInfo storageInfo = repositoryService.getStorageInfo(data);
            LOG.info("Storage info: {}", storageInfo);

            repositoryService.emptyTrashcan(data);

//            FolderInfo folderInfo = repositoryService.getFolderInfo(data, "tignum-releases", Paths.get("/com/tignum"));
//            LOG.info("Folder info: {}", folderInfo);

        } catch (ArtifactoryClientException e) {
            LOG.error("Error when performing the repo method!", e);
        }
    }

    private static void performNodeCalls(ArtifactoryData data, NodeService nodeService) {
        try {
            LOG.info("Ping: {}", nodeService.ping(data));
            LOG.info("SysInfo: {}", nodeService.getSystemInfo(data));
            LOG.info("ServiceId: {}", nodeService.getServiceId(data));
        } catch (ArtifactoryClientException e) {
            LOG.error("Error when performing the node method!", e);
        }
    }
}
